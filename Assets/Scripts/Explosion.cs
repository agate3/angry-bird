﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.GetComponent<Rigidbody2D>() == null) return;

        if (col.gameObject.tag == "Enemy" || col.gameObject.tag == "Obstacle")
        {

            Destroy(col.gameObject);
        }
    }

    public void Explode(float power, float radius){
        Vector2 explosionPos = transform.position;
        float computedRadius = radius * gameObject.GetComponent<CircleCollider2D>().radius;
        // Debug.Log(computedRadius + "|" + radius + "|" + gameObject.GetComponent<CircleCollider2D>().radius);
        Collider2D[] colliders = Physics2D.OverlapCircleAll(explosionPos, computedRadius);
        foreach(Collider2D hit in colliders){
            Rigidbody2D rb = hit.GetComponent<Rigidbody2D>();
            if (rb != null && hit.gameObject.tag != "Bird"){
                Vector2 dir = hit.transform.position - transform.position;
                rb.AddForce(dir * (power * (computedRadius / Vector2.Distance(hit.transform.position,transform.position))));
                // Debug.Log(hit.gameObject.name);
                // Debug.Log(power * (computedRadius / Vector2.Distance(hit.transform.position,transform.position)));
            }
        }
    }

    void Start(){
        Destroy(gameObject, 0.2f);
    }
}
