﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrownBird : Bird
{
    public GameObject Explosion;

    [SerializeField]
    public float _explosionScale = 1f;
    public float _explosionPower = 10f;
    public float _explosionRadius = 1.5f;
    public bool _hasExplode = false;


    public void Explode()
    {
        if ((State == BirdState.Thrown || State == BirdState.HitSomething) && !_hasExplode)
        {
            StartAnimation();
            _hasExplode = true;
        }
    }

    public void StartAnimation(){
        GameObject _explosion = Instantiate(Explosion, gameObject.transform.position, Quaternion.identity);
        _explosion.transform.localScale *= _explosionScale;
        _explosion.GetComponent<Explosion>().Explode(_explosionPower, _explosionRadius);
        Destroy(gameObject);
    }

    public override void OnTap()
    {
        Explode();
    }

    void OnCollisionEnter2D(Collision2D col){
        Explode();
    }
}