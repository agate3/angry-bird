﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public SlingShooter SlingShooter;
    public TrailController TrailController;
    public List<Bird> Birds;
    public List<Enemy> Enemies;
    private Bird _shotBird;
    public BoxCollider2D TapCollider;
    public int level = 0;
    public int totalLevel = 1;

    private bool _isGameEnded = false;
    private GUIStyle guiStyle = new GUIStyle();

    void OnGUI(){
        guiStyle.fontSize = 40;
        
        if (_isGameEnded){
            if (Enemies.Count == 0){
                guiStyle.normal.textColor = Color.green;
                GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 10, 2000, 1000), "YOU WIN", guiStyle);
                if (GUI.Button(new Rect(Screen.width / 2 - 60, 35, 120, 53), "NEXT LEVEL")){
                    StartCoroutine(NextScene(1));
                }
            }else{
                guiStyle.normal.textColor = Color.red;
                GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 10, 2000, 1000), "GAME OVER", guiStyle);
                if (GUI.Button(new Rect(Screen.width / 2 - 60, 35, 120, 53), "RESET")){
                    StartCoroutine(NextScene(0));
                }
            }
        }
    }

    IEnumerator NextScene(int n)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync((level+n)%totalLevel);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    void Start()
    {
        for(int i = 0; i < Birds.Count; i++)
        {
            Birds[i].OnBirdDestroyed += ChangeBird;
            Birds[i].OnBirdShot += AssignTrail;
        }

        for(int i = 0; i < Enemies.Count; i++)
        {
            Enemies[i].OnEnemyDestroyed += CheckGameEnd;
        }

        TapCollider.enabled = false;
        SlingShooter.InitiateBird(Birds[0]);
        _shotBird = Birds[0];
    }

    public void ChangeBird()
    {
        TapCollider.enabled = false;

        if (_isGameEnded)
        {
            return;
        }
        
        Birds.RemoveAt(0);

        if(Birds.Count > 0)
        {
            SlingShooter.InitiateBird(Birds[0]);
            _shotBird = Birds[0];
        } else {
            StartCoroutine(CheckGameOver());
        }
    }

    IEnumerator CheckGameOver(){
        yield return new WaitForSeconds(3);
        if (!_isGameEnded){
            _isGameEnded = true;
        }
    }

    public void CheckGameEnd(GameObject destroyedEnemy)
    {
        for(int i = 0; i < Enemies.Count; i++)
        {
            if(Enemies[i].gameObject == destroyedEnemy)
            {
                Enemies.RemoveAt(i);
                break;
            }
        }

        if(Enemies.Count == 0)
        {
            _isGameEnded = true;
        }
    }

    public void AssignTrail(Bird bird)
    {
        TrailController.SetBird(bird);
        StartCoroutine(TrailController.SpawnTrail());
        TapCollider.enabled = true;
    }

    void OnMouseUp()
    {
        if(_shotBird != null)
        {
            _shotBird.OnTap();
        }
    }
}